require_relative "legs"
require_relative "arms"
require_relative "torso"
require_relative "hips"

class Buddy

  def initialize
    @knees = Knees.new
    @torso = Torso.new
    @arms = Arms.new
    @hips = Hips.new
    @legs = Legs.new
  end

  # The bench press method
  def bench_press(sets, repeats)
    sets.times do |i|
      puts "\nOk, bench press - set #{i} out of #{sets}, here we go!"
      puts "Laying down on the bench..."
      @knees.bend
      @torso.keep_streight
      @arms.grab_bar
      repeats.times do
        @torso.lower
        @arms.extend
        @torso.push
      end
      @arms.put_bar_away
      puts "Rest"
    end
  end

  # The squat method
  def squat(sets, repeats)
    sets.times do |i|
      puts "\nOk, squats - set #{i} out of #{sets}, here we go!"
      repeats.times do
        @arms.grab_bar
        @hips.move_hips_back
        @knees.bend
        @torso.lower
        @knees.bend
        @hips.move_hips_back
      end
      puts "Resting"
    end
  end

  def biceps_curl(sets, repeats)
    sets.times do |i|
      puts "Ok, biceps curl - set #{i+1} out of #{sets}, here we go!"
      @arms.grab_one_hand
      left_hand_repeats = repeats/2
      left_hand_repeats.times { @arms.curl('left')}
      @arms.switch_hand
      # If the number of repeats is odd, then the second hand must do one repeat more
      (repeats - left_hand_repeats).times { @arms.curl('right')}
      @arms.put_dumbell_away('right')
      puts "Resting"
    end
  end


  def workout(workout_plan)
    workout_plan.each do |day, exercise|
      puts "\n-----------------------------"
      puts "It's #{day.to_s}, time for some exercise!"
      puts "-----------------------------"
      exercise.each_key {|method| send(method, exercise.values.flatten[0], exercise.values.flatten[1])}
    end
  end

end






