class Arms
  def grab_bar
    puts "Grabbing the bar..."
  end

  def lower_bar
    puts "Lowering the bar to the chest..."
  end

  def extend
    puts "Extending arms..."
  end

  def put_bar_away
    puts "Putting the bar away."
  end

  def grab_one_hand
    puts "Grabbing with one hand..."
  end

  def curl(arm)
    puts "Curling #{arm} arm..."
  end

  def switch_hand
    puts "Switching hand..."
  end

  def put_dumbell_away(arm)
    puts "Putting the dumbell away with my #{arm} arm."
  end
end
