class Legs
  def bend
    puts "Bending legs..."
  end
end

class Knees < Legs
  def bend
    puts "Bending knees..."
  end
end
