require_relative "body/buddy"

workout_plan = {
        monday: {
            bench_press: [4, 10],
        },
        wednesday: {
            squat: [5, 25],
        },
        friday: {
            biceps_curl: [4, 24]
        }
    }

Buddy.new.workout(workout_plan)


